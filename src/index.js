import React from 'react'
import ReactDOM from 'react-dom'
// import PrimeiroComponente from './componentes/PrimeiroComponente'
// import { CompA, CompB } from './componentes/DoisComponentes'
// import MultiElementos from './componentes/MultiElementos'
// import FamiliaSilva from './componentes/FamiliaSilva'
// import Familia from './componentes/Familia'
// import Membro from './componentes/Membro'
// import ComponenteComoFuncao from './componentes/ComponenteComFuncao'
// import Pai from './componentes/Pai'
// import ComponenteClasse from './componentes/ComponenteClasse'
import Contador from './componentes/Contador'

const elemento = document.getElementById('root')
ReactDOM.render(
  <div>
    { <Contador /> }
    {/* <ComponenteClasse valor="Sou um componente de classe!"/> */}
    {/* <Pai /> */}
    {/* <ComponenteComoFuncao /> */}
    {/* <Familia sobrenome="Pereira">
      <Membro nome="Andre" />
      <Membro nome="Mariana" />
    </Familia> */}
    {/* <Familia >
      <Membro nome="Andre" sobrenome="Pereira" />
      <Membro nome="Mariana" sobrenome="Pereira" />
    </Familia> */}
    {/* <FamiliaSilva /> */}
    {/* <MultiElementos /> */}
    {/* <CompA valor="Olá eu sou A!" />
    <CompB valor="B na área!" /> */}
    {/* <PrimeiroComponente valor="Bom dia!" /> */}

  </div>
  , elemento)

// const jsx = <h1>Olá React!</h1>
// ReactDOM.render(jsx, elemento)

// ReactDOM.render(
//   <ul>
//     <li>1) Pedro</li>
//     <li>2) Maria</li>
//     <li>3) Ana</li>
//   </ul>
//   , elemento)